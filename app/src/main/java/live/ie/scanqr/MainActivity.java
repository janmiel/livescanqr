package live.ie.scanqr;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;



import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import live.ie.scanqr.EventDetails;
import live.ie.scanqr.R;


public class MainActivity extends AppCompatActivity {

    public String serverUrl = "https://ticketstop.ie/qrscanner/qrcodescanner.php";

    EventDetails eventDetails = new EventDetails();


    String currentYear = "";
    String currentMonth = "";
    String currentDay = "";

    boolean workOffline = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // modify action bar
        ActionBar actionBar = getSupportActionBar();

        actionBar.setIcon(R.drawable.icon);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.logo));
        actionBar.show();

        //setViewElementValue("Button" , R.id.pushToServerButton, "active", "false");


        //check if there is existing session
        if (eventDetails.getEventDetails().containsKey("scanningAllowed")) {

            setViewElementValue("TextView", R.id.outputField, null, "" + "Number of tickets:" + eventDetails.getEventDetails().get("ticketsQty"), 0);

        } else {
            // try to restore previous session
            boolean sessionRestored = restorePreviousSessionIfExists();
            if (sessionRestored == true) {
                setViewElementValue("TextView", R.id.outputField, null, eventDetails.getEventDetails().get("eventName") +"\n" + "Number of tickets:" + eventDetails.getEventDetails().get("numberOfTickets"), 0);
            } else {
                // restoring session did not work

            }
        }


        // add liestener to manual ticket edittext
        EditText editText = (EditText) findViewById(R.id.ticketNumberManual);
        editText.addTextChangedListener(mTextEditorWatcher);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    // menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {

            case R.id.menu_item1:
                scanQrCode(null);
                return true;

            case R.id.menu_item3:
                readControlNumber();
                return true;

            case R.id.menu_item4:
                if (clearMemory()) {
                    Toast.makeText(getApplicationContext(), "Memory purged;", Toast.LENGTH_LONG).show();
                    setViewElementValue("TextView", R.id.outputField, null, "", 0);
                    setViewElementValue("TextView", R.id.outputField2, null, "", 0);
                }
                return true;




            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // end menu


    /////////////////////////////////////////////////////
    // // get setup settings from setup qr code
    ////////////////////////////////////////////////////

    // scan QR code
    public void scanQrCode(View v) {

        try {
            //Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            //ntent.putExtra("SCAN_MODE", "PRODUCT_MODE"); // "PRODUCT_MODE for bar codes ; QR_CODE_MODE for qr code
            //startActivityForResult(intent, 0);

            IntentIntegrator integrator = new IntentIntegrator(this);
            //integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
            //integrator.setPrompt("Scan a barcode");
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.initiateScan();
        } catch (Exception e) {

        }

    }

    public void manualTicket(View v) {

        EditText editText = (EditText) findViewById(R.id.ticketNumberManual);

        String ticketNumber = "TS" + editText.getText().toString().trim();

        if (!ticketNumber.equals("TS") && !ticketNumber.equals("")) {

            scannerActivity(ticketNumber);

            editText.setText("");

        }

    }

    public void readControlNumber() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Manual Device Setup");
        alert.setMessage("Enter Control Number");

// Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Do something with value!
                String setupNumber = input.getText().toString();
                if (ifStringContains(setupNumber)) {
                    String[] controlNumber = setupNumber.split("/");
                    if (!controlNumber[0].equals("") && !controlNumber[1].equals("")) {

                        String uniqueString = "ucanchangeit!";
                        try {
                            String md5 = MD5(controlNumber[0].toString() + uniqueString);
                            String controlCode = md5.substring(md5.length() - 4);

                            if (controlCode.equals(controlNumber[1])) {
                                String setupDevice = "action=setupDevice&eventId=" + controlNumber[0];
                                scannerActivity(setupDevice);
                            } else {
                                setViewElementValue("TextView", R.id.outputField, null, "Check setup Control Number", 0);
                            }


                        } catch (NoSuchAlgorithmException e) {

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Code empty or wrong.", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Code empty or wrong.", Toast.LENGTH_LONG).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);


        if (scanResult.getContents() != null) {


            String urlUri = intent.getStringExtra("SCAN_RESULT");


            scannerActivity(urlUri);


        } else {

        }
    }

    // what to do with scanned string
    private void scannerActivity(String urlUri) {

        Map<String, String> mapResult = new HashMap<>();

        //check if it is url or just a string

        if (urlUri.contains("&")) {


            mapResult = nvpToMap(urlUri);

        } else if (eventDetails.getEventDetails().containsKey("scanningAllowed")) {
            if (eventDetails.getEventDetails().get("scanningAllowed").equals("true")) {


                mapResult.put("action", "scanTicket");
                mapResult.put("ticketNumber", urlUri);


            }
        } else {

            Toast.makeText(getApplicationContext(), "Scan 'setup device' QR code first", Toast.LENGTH_LONG).show();

        }

        // action for device setup
        if (!mapResult.equals(null) && mapResult.get("action") != null && mapResult.get("action").equals("setupDevice")) {

            String temp = mapResult.get("eventId").toString() + "ucanchangeit!";


            String controlNumber = "";
            try {
                controlNumber = MD5(temp);
            } catch (NoSuchAlgorithmException e) {

            }


            if (isNetworkAvailable()) {

                AsyncConnectSetupDevice response = new AsyncConnectSetupDevice();
                response.execute(serverUrl, urlUri + "&controlNumber=" + controlNumber);
            } else {

                showTicketValidation("hide");

                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
            }


        }
        // action for ticket scan
        else if (!mapResult.equals(null) && mapResult.get("action") != null && mapResult.get("action").equals("scanTicket")) {


            if (eventDetails.getEventDetails().containsKey("scanningAllowed") && eventDetails.getEventDetails().get("scanningAllowed").equals("true")) {
                if (workOffline) {

                    TextView outputField = (TextView) findViewById(R.id.outputField2);
                    outputField.setSingleLine(false);
                    // set event details


                    if (eventDetails.getEventDetails().containsKey(mapResult.get("ticketNumber"))) {

                        if (eventDetails.getEventDetails().get(mapResult.get("ticketNumber").toString()).equals("valid")) {

                            String ticketNumber = mapResult.get("ticketNumber").toString();

                            outputField.setText(ticketNumber + "\nTicket is valid.");

                            showTicketValidation("valid");

                            eventDetails.setEventDetails(ticketNumber, "scanned");

                            //int numOfTickets = Integer.valueOf(eventDetails.getEventDetails().get("numberOfScannedTickeets")) + 1;

                            //eventDetails.setEventDetails("numberOfScannedTickets", String.valueOf(numOfTickets));
                            //setViewElementValue("TextView" , R.id.numberOfScannedTickets, null,  "Number of scanned tickets: " + eventDetails.getEventDetails().get("numberOfScannedTickets") , 0);

                            writeToFile(eventDetails.getEventDetails());

                        } else {

                            outputField.setText(mapResult.get("ticketNumber").toString() + "\nTICKET ALREADY USED.");

                            showTicketValidation("invalid");

                        }


                    } else {

                        outputField.setText(mapResult.get("ticketNumber").toString() + "\nTicket NOT for this event.");


                        showTicketValidation("invalid");
                    }


                }

                if (workOffline == false) {
                    if (isNetworkAvailable()) {

                        logger("am sending ticket for validation" + mapResult.get("ticketNumber"));
                        String scanTicketUrl = "http://staging.ticketstop.live/liveScanner.php?scanTicket";
                        AsyncConnectScanTicket response = new AsyncConnectScanTicket();
                        response.execute(scanTicketUrl, "action=scanTicket&eventId=" + eventDetails.getEventDetails().get("eventId") + "&ticketNumber=" + mapResult.get("ticketNumber") );
                    } else {

                        showTicketValidation("hide");
                        Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "Scan 'setup device' QR code first", Toast.LENGTH_LONG).show();
            }

        } else {

            Toast.makeText(getApplicationContext(), "QR Code is not correct", Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://live.ie.scanqr/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://live.ie.scanqr/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /////////////////////////////////////////
    // this class is setting up device
    ////////////////////////////////////////
    private class AsyncConnectSetupDevice extends AsyncTask<String, String, String> {

        private String result;


        @Override
        protected String doInBackground(String... params) {

            try {
                // Do your long operations here and return the result
                URL url;
                url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                String charset = "UTF-8";
                urlConnection.setRequestMethod("POST");


                String query = params[1];


                byte[] postData = query.getBytes(StandardCharsets.UTF_8);

                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Charset", charset);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                wr.write(postData);

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = readStream(in);


            } catch (Exception e) {

                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();

            } finally {

            }
            return result;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation

            Map<String, String> mapResult = nvpToMap(result);



            // check response from server

            if (mapResult.get("action") != null) {
                if (mapResult.get("action").equals("setupDevice") && mapResult.get("response").equals("true") && mapResult.get("validation").equals("true")) {

                    // set event details


                    eventDetails.setEventDetails("eventId", mapResult.get("eventId").toString());
                    eventDetails.setEventDetails("eventDate", currentDay + currentMonth + currentYear);
                    eventDetails.setEventDetails("scanningAllowed", "true");
                    eventDetails.setEventDetails("numberOfScannedTickets", "0");

                    String ticketNumbers = mapResult.get("ticketNumbers").toString();



                    Integer numberOfTickets = 0;

                    try {

                        logger(result);

                        String ticketStopLiveUrl = "http://staging.ticketstop.live/liveScanner.php?saveTicketNumbers";

                        AsyncConnectSendTicketsToTicketstopLive responseTSLive = new AsyncConnectSendTicketsToTicketstopLive();
                        responseTSLive.execute(ticketStopLiveUrl, result);



                    } catch (Exception e) {

                        Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
                        Log.d("network", e.toString());

                    }

                    setScanButtonVisible();

                    String eventName = null;

                    try {
                        eventName = URLDecoder.decode(mapResult.get("eventName").toString(), "UTF-8");
                        eventDetails.setEventDetails("eventName", eventName);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }



                    setViewElementValue("TextView", R.id.outputField2, null, "", 0);

                    showTicketValidation("hide");

                    // save session and allow scanning
                    writeToFile(eventDetails.getEventDetails());


                } else if (mapResult.get("validation").equals("false")) {

                    setViewElementValue("TextView", R.id.outputField, null, "No tickets for this event", 0);

                }


            }


        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog

            setViewElementValue("TextView", R.id.outputField, null, "Connecting ...", 0);

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {


            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }
    //end of Scan QR code


    /////////////////////////////////////////////////////
    // end // get setup settings from setup qr code
    ////////////////////////////////////////////////////


    private class AsyncConnectSendTicketsToTicketstopLive extends AsyncTask<String, String, String> {

        private String result;


        @Override
        protected String doInBackground(String... params) {

            try {
                // Do your long operations here and return the result
                URL url;
                url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                String charset = "UTF-8";
                urlConnection.setRequestMethod("POST");


                String query = params[1];


                byte[] postData = query.getBytes(StandardCharsets.UTF_8);

                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Charset", charset);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                wr.write(postData);

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = readStream(in);


            } catch (Exception e) {



            } finally {

            }
            return result;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation

            try {

                logger(result.toString());
                if (result == null) {

                } else {
                    result = result;


                    Map mapResult = nvpToMap(result.toString());

                    logger(mapResult.toString());

                    eventDetails.setEventDetails("numberOfTickets", mapResult.get("numberOfTickets").toString());

                    setViewElementValue("TextView", R.id.outputField, null, "Event added:\n" + eventDetails.getEventDetails().get("eventName") + "\nNumber of tickets:" + mapResult.get("numberOfTickets"), 0);

                    // check response from server
                    //
                    //            if (result.get("action") != null) {
                    //
                    //

                    //            }

                }
            }catch(Exception e){
                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_LONG).show();
            }
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog

            setViewElementValue("TextView", R.id.outputField2, null, "Connecting ...", 0);

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {


            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }
    //end of Send tickets to ticketstiop.live
    ///////////////////////////////////////////////////
    // ticket scan
    ///////////////////////////////////////////////////
    private class AsyncConnectScanTicket extends AsyncTask<String, String, String> {

        private String result;

        @Override
        protected String doInBackground(String... params) {

            try {
                // Do your long operations here and return the result
                URL url;
                url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                String charset = "UTF-8";
                urlConnection.setRequestMethod("POST");


                String query = params[1];


                byte[] postData = query.getBytes(StandardCharsets.UTF_8);

                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Charset", charset);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                wr.write(postData);

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = readStream(in);


            } catch (Exception e) {

                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();

            } finally {

            }
            return result;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            Map mapResult = nvpToMap(result);
            logger(result);
            // check response from server

            if (mapResult.get("status") != null) {
                if (mapResult.get("status").equals("true")) {

                    // set event details


                    setViewElementValue("TextView", R.id.outputField2, null, mapResult.get("ticketNumber").toString() + "\nTicket is valid.", Color.GREEN);
                    setViewElementValue("TextView", R.id.numberOfScannedTickets, null, "Scanned:" + mapResult.get("ticketsScanned").toString(), 0);

                    showTicketValidation("valid");

                }
                if (mapResult.get("status").equals("false")) {

                    setViewElementValue("TextView", R.id.outputField2, null, mapResult.get("ticketNumber").toString() + "\nTicket is NOT valid.", Color.RED);


                    showTicketValidation("invalid");


                }
                if (mapResult.get("status").equals("alreadyUsed")) {





                    setViewElementValue("TextView", R.id.outputField2, null, mapResult.get("ticketNumber").toString() + "\nTicket already used \n" + mapResult.get("timestamp"), Color.RED);


                    showTicketValidation("invalid");


                }


            }


            // do something with the result
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
            setViewElementValue("TextView", R.id.outputField2, null, "Connecting ...", 0);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {


            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }
    ///////////////////////////////////////////////////
    // end // ticket scan
    ///////////////////////////////////////////////////


    // put data to server
    private void pushToServer() throws UnsupportedEncodingException {


        if (eventDetails.getEventDetails().containsKey("eventId")) {

            if (isNetworkAvailable()) {

                String urlUri = "action=pushToServer";

                Map<String, String> map = eventDetails.getEventDetails();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    urlUri += "&" + entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");
                }


                String temp = eventDetails.getEventDetails().get("eventId") + "ucanchangeit!";
                String controlNumber = "";
                try {
                    controlNumber = MD5(temp);
                } catch (NoSuchAlgorithmException e) {

                }

                AsyncConnectPushToServer response = new AsyncConnectPushToServer();
                response.execute(serverUrl, urlUri + "&controlNumber=" + controlNumber);
            } else {

                showTicketValidation("hide");

                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
            }
        } else {

            Toast.makeText(getApplicationContext(), "Nothing to send.", Toast.LENGTH_LONG).show();

        }

    }

    private class AsyncConnectPushToServer extends AsyncTask<String, String, String> {

        private String result;

        @Override
        protected String doInBackground(String... params) {

            try {
                // Do your long operations here and return the result

                URL url;
                url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                String charset = "UTF-8";
                urlConnection.setRequestMethod("POST");

                String query = params[1];


                byte[] postData = query.getBytes(StandardCharsets.UTF_8);

                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Charset", charset);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                wr.write(postData);
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = readStream(in);


            } catch (Exception e) {

                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();

            } finally {

            }
            return result;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation

            logger(result);

            if (result.equals("dataReceived")) {

                // check response from server

                setViewElementValue("TextView", R.id.outputField, null, "Data send to Ticketstop", 0);


            } else {
                setViewElementValue("TextView", R.id.outputField, null, "Data NOT sent", 0);
            }
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog
            setViewElementValue("TextView", R.id.outputField, null, "Connecting ...", 0);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {


            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }


    // read steam from http connection

    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();

        return sb.toString();
    }

    public Map nvpToMap(String urlUri) {

        Map<String, String> scanResult = new HashMap<String, String>();
        String[] tmp = urlUri.split("&");


        for (String tm : tmp) {

            if (tm.contains("=")) {

                String[] t = tm.split("=");
                scanResult.put(t[0], t[1]);

            } else {
                scanResult.put(null, null);
                break;
            }

        }


        return scanResult;

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private String MD5(String input) throws NoSuchAlgorithmException {


        String result = input;
        if (input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while (result.length() < 32) { //40 for SHA-1
                result = "0" + result;
            }
        }
        return result;

    }

    private boolean checkIfFileExists() {

        Context context = getApplicationContext();
        File myFile = new File(context.getFilesDir(), "savedSession.ser");

        if (myFile.isFile()) {
            return true;
        } else {
            return false;
        }

    }

    private void writeToFile(HashMap data) {
        try {
            Context context = getApplicationContext();
            File myFile = new File(context.getFilesDir(), "savedSession.ser");
            FileOutputStream fos = new FileOutputStream(myFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
            fos.close();


        } catch (IOException e) {
            Log.d("ScanQR", "File write failed: " + e.toString());
        }
    }

    private HashMap<String, String> readFromFile() {

        HashMap<String, String> map = null;

        try {
            Context context = getApplicationContext();
            File myFile = new File(context.getFilesDir(), "savedSession.ser");
            FileInputStream fis = new FileInputStream(myFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();


        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
        } catch (IOException e) {
            Log.d("ScanQR", "Can not read file: " + e.toString());
        }

        return map;
    }

    private void setScanButtonVisible() {
        Button nextScannButton = (Button) findViewById(R.id.nextScanButton);
        if (nextScannButton != null) {
            nextScannButton.setEnabled(true);


        }
    }

    private boolean restorePreviousSessionIfExists() {


        Date date = new Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        this.currentYear = String.valueOf(cal.get(Calendar.YEAR));
        this.currentMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
        this.currentDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));


        if (checkIfFileExists()) {

            HashMap<String, String> previousSession = readFromFile();
            //check if session is from today

            // remove this comments too allow only sessions from the same date

            // if(previousSession.containsKey("eventDate")){
            //     if(previousSession.get("eventDate").equals(currentDay.toString()+currentMonth.toString()+currentYear.toString())){

            eventDetails.eventDetails = previousSession;

            Toast.makeText(getApplicationContext(), "Previous Session Restored", Toast.LENGTH_LONG).show();
            setScanButtonVisible();

            //     }
            // }

            return true;
        } else {
            return false;
        }
    }

    private boolean clearMemory() {

        if (checkIfFileExists()) {

            Context context = getApplicationContext();
            File myFile = new File(context.getFilesDir(), "savedSession.ser");
            eventDetails.eventDetails = new HashMap<String, String>();
            myFile.delete();
            return true;
        } else {
            return false;
        }

    }

    private boolean ifStringContains(String string) {

        Boolean result = string.contains("/");

        return result;

    }

    private void showTicketValidation(String setStatus) {

        if (setStatus == "hide") {
            ImageView showTicketValidation = (ImageView) findViewById(R.id.showTicketValidation);
            showTicketValidation.setImageDrawable(null);
        }
        if (setStatus == "valid") {

            ImageView showTicketValidation = (ImageView) findViewById(R.id.showTicketValidation);
            showTicketValidation.setImageResource(R.drawable.valid);

        }
        if (setStatus == "invalid") {

            ImageView showTicketValidation = (ImageView) findViewById(R.id.showTicketValidation);
            showTicketValidation.setImageResource(R.drawable.invalid);

        }

    }


    private void setViewElementValue(String elementType, int elementName, String option, String elementValue, int textColor) {

        if (elementType.equals("TextView")) {


            TextView element = (TextView) findViewById(elementName);

            if (textColor != 0) {
                element.setTextColor(textColor);
            }
            element.setText(elementValue);
        }

        if (elementType.equals("Button")) {

            if (option.equals("active")) {
                Button element = (Button) findViewById(elementName);
                element.setEnabled(Boolean.valueOf(elementValue));
            }
        }

    }

    private void logger(String logText) {

        Log.d("ScanQR", logText);
    }


    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        boolean dashAdded = false;

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length


            if (s.length() == 6 && dashAdded == false) {
                logger(s.toString());

                EditText editText = (EditText) findViewById(R.id.ticketNumberManual);

                editText.append("-");
                dashAdded = true;
            }
            if (dashAdded == true && s.length() < 6) {
                dashAdded = false;
            }

            // add TS if empty
            if (s.length() == 0) {

                EditText editText = (EditText) findViewById(R.id.ticketNumberManual);

                editText.append("TS");
            }
        }

        public void afterTextChanged(Editable s) {
        }
    };


}


