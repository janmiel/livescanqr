package live.ie.scanqr;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by jmielewczyk on 17/04/2016.
 */
public class EventDetails {

    public HashMap<String , String> eventDetails = new HashMap<String , String>();
    public boolean scanningAllowed = false;


    public void setEventDetails(String name , String value){
        this.eventDetails.put(name , value);
    }

    public HashMap<String, String> getEventDetails(){

        return this.eventDetails;
    }




}
